PATH="${HOME}/.anyenv/bin:$PATH"
eval "$(anyenv init -)"

# pyenv
# https://github.com/pyenv/pyenv/issues/1906
#
#PYENV_ROOT="$HOME/.anyenv/envs/pyenv"
#PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init --path)"
fi
