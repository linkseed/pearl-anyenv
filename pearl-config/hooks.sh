# anyenv - all in one for **env
# https://github.com/riywo/anyenv

export ANYENV_ROOT="${HOME}/.anyenv"

function post_install() {
  install_or_update_git_repo https://github.com/riywo/anyenv.git \
    ${ANYENV_ROOT} master

  ## manifest
  mkdir -p ${HOME}/.config/anyenv
  install_or_update_git_repo https://github.com/anyenv/anyenv-install.git \
    ${HOME}/.config/anyenv/anyenv-install master

  ## plugins:
  mkdir -p ${ANYENV_ROOT}/plugins
  # anyenv-update:
  install_or_update_git_repo https://github.com/znz/anyenv-update.git \
    ${ANYENV_ROOT}/plugins/anyenv-update master
  # anyenv-git:
  install_or_update_git_repo https://github.com/znz/anyenv-git.git \
    ${ANYENV_ROOT}/plugins/anyenv-git master

  [ ! -e ${HOME}/.profile ] && touch ${HOME}/.profile
  apply "source ${PEARL_PKGDIR}/profile" "${HOME}/.profile"

  warn "Some *envs are not compatible with fish"
  info "Restart your shell so that PATH changes take effect."
  return 0
}

function post_update() {
  post_install
}

function pre_remove() {
  rm -rf ${ANYENV_ROOT}
  rm -rf ${HOME}/.config/anyenv

  unapply "source ${PEARL_PKGDIR}/profile" "${HOME}/.profile"
  return 0
}

# vim: set ts=2 sw=2 et:
